<section class="footer">

   <div class="box-container">

     

      <div class="box">
         <h3>contact info</h3>
         <p> <i class="fas fa-phone"></i> +40723478011 </p>
         <p> <i class="fas fa-phone"></i> +40789365842 </p>
         <p> <i class="fas fa-envelope"></i> lily_guitargh@gmail.com </p>
         <p> <i class="fas fa-map-marker-alt"></i> Bucharest, Romania </p>
      </div>

      <div class="box">
         <h3>follow us</h3>
         <a href="#"> <i class="fab fa-facebook-f"></i> facebook </a>
         <a href="#"> <i class="fab fa-twitter"></i> twitter </a>
         <a href="#"> <i class="fab fa-instagram"></i> instagram </a>
         <a href="#"> <i class="fab fa-linkedin"></i> linkedin </a>
      </div>

   </div>

</section>