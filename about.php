<?php

include 'config.php';

session_start();

$user_id = $_SESSION['user_id'];

if(!isset($user_id)){
   header('location:login.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>About</title>

   <!-- font awesome cdn link  -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

   <!-- custom css file link  -->
   <link rel="stylesheet" href="css/style2.css">

</head>
<body>
   
<?php include 'header.php'; ?>

<div class="heading">
   <h3>About us</h3>
</div>

<section class="about">

   <div class="flex">

      <div class="image">
         <img src="images/about.avif" alt="">
      </div>

      <div class="content">
         <h3>why GuitArgh?</h3>
         <p>Our guitars are handcrafted by skilled artisans and luthiers who pour their expertise and passion into every instrument. Each piece is a masterpiece, ensuring exceptional quality and exquisite sound.</p>
         <p> GuitArgh is not just a shop; it's a vibrant community of music lovers. When you choose us, you join a network of like-minded individuals, sharing experiences, tips, and inspiration. </p>
         <a href="contact.php" class="btn">contact us</a>
      </div>

   </div>

</section>

<section class="reviews">

   <h1 class="title">client's reviews</h1>

   <div class="box-container">

      <div class="box">
         <img src="images/pic-1.png" alt="">
         <p> The staff's expertise helped me find the perfect guitar, tailored to my style. The sense of community here is palpable; it's not just a shop, but a place where music lovers unite. The service was exceptional, making my experience memorable. I left with not just a guitar but a newfound inspiration. GuitArgh is a haven indeed!</p>
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star-half-alt"></i>
         </div>
         <h3>Chris</h3>
      </div>

      <div class="box">
         <img src="images/pic-2.png" alt="">
         <p>GuitArgh is a true gem for any music enthusiast! The moment I stepped in, I was greeted by a warm, knowledgeable staff eager to assist. The selection of guitars is unparalleled, catering to various tastes and budgets. I was amazed by the craftsmanship and quality of their instruments. I've found my musical home at GuitArgh!</p>
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star-half-alt"></i>
         </div>
         <h3>Anna</h3>
      </div>

   </div>

</section>


<?php include 'footer.php'; ?>

<!-- custom js file link  -->
<script src="js/script.js"></script>

</body>
</html>