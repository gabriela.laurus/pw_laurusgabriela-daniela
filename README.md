# PW_LaurusGabriela-Daniela

## Tehnologii Utilizate:
- Frontend:
  - HTML pentru structura paginilor web.
  - CSS pentru stilizare și design responsiv.
  - JavaScript pentru interactivitate și validare de client.

- Backend:
  - PHP pentru gestionarea serverului și logica de backend.
  - MyAdminPHP pentru stocarea datelor despre produse, utilizatori și comenzi.

